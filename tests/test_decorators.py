# Copyright 2021 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import click
import pytest

from xmlhelpy import argument
from xmlhelpy import command
from xmlhelpy import environment
from xmlhelpy import group
from xmlhelpy import option


@pytest.mark.parametrize("param_decorator", [argument, option])
def test_duplicate_names(param_decorator):
    """Test if duplicate parameter names are checked correctly."""
    error_msg = "tests.test_decorators.<lambda>: Name '{}' specified twice."

    # Test the builtin "--commands" option for groups.
    with pytest.raises(ValueError) as e:
        group()(param_decorator("commands")(lambda: None))

    assert str(e.value) == error_msg.format("commands")

    # Test the builtin "--env-exec" option for environments.
    with pytest.raises(ValueError, match="test") as e:
        environment()(param_decorator("env-exec")(lambda: None))

    assert str(e.value) == error_msg.format("env-exec")

    # Test the builtin "--version" option for all command types.
    for cmd_decorator in [group, command, environment]:
        with pytest.raises(ValueError) as e:
            cmd_decorator()(param_decorator("version")(lambda: None))

        assert str(e.value) == error_msg.format("version")

    # Test the builtin "--xmlhelp" option for commands and environments.
    for cmd_decorator in [command, environment]:
        with pytest.raises(ValueError) as e:
            cmd_decorator()(param_decorator("xmlhelp")(lambda: None))

        assert str(e.value) == error_msg.format("xmlhelp")

    # Test a user-defined option for all command types.
    for cmd_decorator in [group, command, environment]:
        with pytest.raises(ValueError) as e:
            cmd_decorator()(
                param_decorator("test")(param_decorator("test")(lambda: None))
            )

        assert str(e.value) == error_msg.format("test")


def test_duplicate_chars():
    """Test if duplicate char names for options are checked correctly."""
    with pytest.raises(ValueError) as e:
        command()(option("opt_1", char="o")(option("opt_2", char="o")(lambda: None)))

    assert str(e.value) == "tests.test_decorators.<lambda>: Char 'o' specified twice."


def test_option():
    """Test the specific checks of the option decorator."""
    with pytest.raises(click.BadOptionUsage) as e:
        command()(option("te/st")(lambda: None))

    assert str(e.value) == "Names cannot contain slashes."

    with pytest.raises(click.BadOptionUsage) as e:
        command()(option("test", char="#")(lambda: None))

    assert str(e.value) == "Chars cannot contain special characters."

    with pytest.raises(click.BadOptionUsage) as e:
        command()(option("test", var_name="import")(lambda: None))

    assert str(e.value) == "Variable names cannot be reserved keywords or identifiers."
