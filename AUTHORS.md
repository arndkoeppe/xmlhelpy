# Authors

Currently maintained by **Nico Brandt**
([nico.brandt@kit.edu](mailto:nico.brandt@kit.edu)).

List of contributions from the Kadi4Mat team and other contributors, ordered by
date of first contribution:

* **Nico Brandt**
* **Lars Griem**
* **Ephraim Schoof**
