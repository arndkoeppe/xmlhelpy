import os

from setuptools import find_packages
from setuptools import setup


with open(os.path.join("xmlhelpy", "version.py"), encoding="utf-8") as f:
    exec(f.read())


with open("README.md", encoding="utf-8") as f:
    long_description = f.read()


setup(
    name="xmlhelpy",
    version=__version__,
    license="Apache-2.0",
    author="Karlsruhe Institute of Technology",
    description="CLI wrapper for the xmlhelp interface.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/iam-cms/workflows/xmlhelpy",
    packages=find_packages(exclude=("tests", "tests.*")),
    include_package_data=True,
    python_requires=">=3.6,<3.11",
    zip_safe=False,
    classifiers=[
        "Development Status :: 4 - Beta",
        "License :: OSI Approved :: Apache Software License",
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 3",
    ],
    install_requires=[
        "click>=7.0.0,<9.0.0",
        "lxml<5.0.0",
    ],
    extras_require={
        "dev": [
            "black==22.3.0",
            "build==0.7.0",
            "pre-commit==2.17.0",
            "pylint==2.13.7",
            "pytest==7.0.1",
            "tox==3.25.0",
            "twine==3.8.0",
        ]
    },
)
